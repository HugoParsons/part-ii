import argparse
import numpy as np
import lmfit
import matplotlib.pyplot as plt
from scipy import signal, ndimage
#a script for fitting scattering data to a sum of Gaussians.

N_e = 18  #input number of electrons here

def Gauss(p,x):
    A = p[0]
    B = p[1]
    C = p[2]
    y = A*np.exp(-B*(x-C)**2)
    return y

def GaussSum(pars,x):
    n = len(pars)//3
    model = np.zeros_like(x)
    for i in range(n):
        p = np.zeros(3)
        p[0] = pars['amp'+str(i)]
        p[1] = pars['width'+str(i)]
        p[2] = pars['position'+str(i)]
        model += Gauss(p,x)
    return model

def residual(pars,x,y):
    r = GaussSum(pars,x) - y
    return r


#argument parser
parser = argparse.ArgumentParser(description='Fits Data to a Gaussian or Lorentzian')
parser.add_argument("-n", "--number", help="number of functions to use in sum")
parser.add_argument("-l", "--lorentzian",action="store_true", help="use a sum of Lorentzians, gaussian is default")
parser.add_argument("-i", "--individual", action="store_true", help = "plot fitted functions individually")
parser.add_argument("-t", "--tolerance", help="tolerance on gaussian functions")
parser.add_argument("-p","--peakfind",action="store_true", help="find peaks?")
parser.add_argument("-z", "--zeropoint",action="store_true",help="Add peak at q=0?")
args = parser.parse_args()

#read in data and truncate
truncation = 5
lotrunc = 0
data = np.genfromtxt("total_1_1.dat")
truncdata = data[int(lotrunc*100):int(truncation*100),:]
np.savetxt("truncdata.txt",truncdata)
truncdata = np.vstack(([0,N_e**2-N_e],truncdata))
x_orig = data[ :,0]
x = truncdata[ :,0]
y = truncdata[ :,1]
#y = y*np.random.normal(1,0.001,len(y)) #add random noise

#Peak Finding
if args.peakfind == True:
    smooth = ndimage.gaussian_filter1d(y, 3)
    enhanced = y - smooth
    peaks, properties = signal.find_peaks(enhanced)
else:
    peaks=[]

params = lmfit.Parameters()
if args.lorentzian == False:                        #Gaussian Optimisation
    if args.peakfind == True:
        for i in range(len(peaks)):
            yi = y[peaks[i]]
            xi = x[peaks[i]]
            t = int(args.tolerance)/100
            params.add('amp'+str(i),value = yi, min =0)
            params.add('width'+str(i),value = 1,min = 0.01)
            params.add('position'+str(i),value = xi,min = xi - xi*t, max = xi + xi*t)
    
    for i in range(int(args.number)):
        n = len(peaks)+i
        params.add('amp'+str(n), value = 1, min = 0)
        params.add('width'+str(n),value = 1, min = 0.01)
        params.add('position'+str(n),value = 1, min = 0, max = truncation)
    
    nfunc = int(args.number) + len(peaks)
    if args.zeropoint == True:
        nfunc += 1
        max_s = N_e**2 - N_e
        params.add('amp'+str(nfunc-1),value = max_s, min = max_s*0.8, max = max_s*1.1 )
        params.add('width'+str(nfunc-1),value = 2, min = 0.01)
        params.add('position'+str(nfunc-1),value = 0, vary = False)
    
    out = lmfit.minimize(residual, params, args = (x,y)) 
    out.params.pretty_print()
    popt = out.params
    print(np.linalg.norm(out.residual))
    

    plt.title(f"Scattering Data Fitted to {nfunc} Gaussians")
    y_fit = np.array([GaussSum(popt,i) for i in x_orig]) #Extrapolate Model over full range
    
    if args.individual == True:                     #Plot individual Gaussians
        n=len(popt)//3
        for j in range(n):
            p_j = np.zeros(3)
            p_j[0] = popt['amp'+str(j)]
            p_j[1] = popt['width'+str(j)]
            p_j[2] = popt['position'+str(j)]
            y_j = Gauss(p_j,x)
            plt.plot(x, y_j, label = f"Gaussian {j+1}", ls = "dashed")



    with open('fit.dat', 'w') as f:                 #Write extrapolated Gaussians to fit.dat
        for i in range(len(x_orig)):
            f.write(f"{x_orig[i]: 4.15f}    {y_fit[i]: 4.9f} \n")

#else:                                               #Lorentzian Optimisation
 


plt.plot(x,y,label= "Data")
plt.plot(x_orig,y_fit,label= "Fitted Data")
plt.xlabel("\(q / a_0^{-1}\)")
plt.ylabel("I")
plt.legend()
plt.show()
 
