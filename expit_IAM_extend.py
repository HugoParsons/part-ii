import numpy as np
import matplotlib.pyplot as plt
import sys
from scipy import special

#extends scattering data using IAM and an expit function
uptruncation = 5
upscale = 2.3
lotruncation = -100
loscale = 10
origdata = np.genfromtxt(sys.argv[1])
IAMdata = np.genfromtxt(sys.argv[2])
x = origdata[:,0]
splice = -special.expit(upscale*(x-uptruncation)) + special.expit(loscale*(x-lotruncation))


extended = np.zeros_like(origdata)
extended[:,0] = x
extended[:,1] = (splice)*origdata[:,1] + (1-splice)*IAMdata[:,1]

#plt.plot(x,extended[:,1],label = "extended")
#plt.plot(x,origdata[:,1],label = "ab initio")
#plt.plot(x,IAMdata[:,1], label = "IAM")
plt.plot(x,splice, label = "$f_{\mathrm{mix}}$")
plt.plot(x,1-splice, label="$1 - f_{\mathrm{mix}}$")
plt.xlabel("$q / a_0^{-1}$")
plt.ylabel("Proportion of mixing")
plt.legend()
#plt.show()

np.savetxt(str(loscale)+'_'+str(upscale)+'_expit_extended.dat',extended)
