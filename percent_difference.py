import numpy as np
import matplotlib.pyplot as plt
import sys
import scipy.fft as ft

#This script plots the percent intensity difference, as well as the absolute PDF difference for all arguments relative to the first. i.e. 2nd-1st, 3rd-1st,4th-1st, etc.
HF = np.genfromtxt(sys.argv[1])
x = HF[ :,0]
N_e = int(np.roots((1,-1,-HF[0,1]))[0])
print(N_e)
r = ft.rfftfreq(HF.size-1,HF[1,0]-HF[0,0])*np.pi*2
q = HF[:,0]
HFsigma = HF[:,1]
dq = q[1]-q[0]
HFpairdist = np.zeros_like(r)
count = 0

def basiscolour(data_label):
    colour_dict={
            "cc-pVDZ": "tab:blue",
            "cc-pVTZ": "tab:orange",
            "cc-pVQZ": "tab:green",
            "aug-cc-pVDZ": "tab:red",
            "aug-cc-pVTZ": "tab:gray",
            "aug-cc-pVQZ": "tab:cyan"
            }
    return colour_dict.get(data_label)

for i in r:
    integrand = HFsigma*np.sin(i*q)*i*q
    HFpairdist[count] = (1/np.pi)*np.trapz(integrand,dx = dq)
    count += 1



for i in range(len(sys.argv)-2):
    scat = np.genfromtxt(sys.argv[i+2])
    r = ft.rfftfreq(scat.size-1,scat[1,0]-scat[0,0])*np.pi*2
    q = scat[:,0]
    sigma = scat[:,1]
    y = 100*np.subtract(sigma+N_e, HFsigma+N_e)/(HFsigma+N_e)
    dq = q[1]-q[0]
    pairdist = np.zeros_like(r)
    count = 0
    
    for j in r:
        integrand = sigma*np.sin(j*q)*j*q
        pairdist[count] = (1/np.pi)*np.trapz(integrand,dx = dq)
        count += 1
    

    data_label=input("please input label for "+sys.argv[i+2])
    coulombhole = pairdist - HFpairdist
    plot1 = plt.figure(1)
    plt.plot(x,y,label = data_label,color=basiscolour(data_label))
    plt.xlabel("$q / a_0^{-1}$",fontsize=12)
    plt.ylabel("$\% \Delta I$",fontsize=12)
    plt.xlim(0, 25)
    plt.legend()
    plt.tight_layout()
    plot2 = plt.figure(2)
    plt.plot(x,coulombhole,label = data_label,color=basiscolour(data_label))
    plt.xlabel("$r_{12} / a_0$",fontsize=12)
    plt.ylabel("$\Delta P(r_{12})$",fontsize=12)
    plt.xlim(0, 25)
    plt.legend()
    plt.tight_layout()
    print(np.linalg.norm(y))

plt.savefig("percent_coulombhole.py",format="pdf")
plt.show()


