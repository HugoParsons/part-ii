import numpy as np
import matplotlib.pyplot as plt
import sys
#generates IAM when given form factors
with open("geom.xyz") as f:   #initialise geometry
    N = int(f.readline())
    f.readline()
    atoms = []
    coords = np.zeros((N,3))
    for i in range(N):
        a = f.readline().split()
        atoms.append(a[0])
        for j in range(3):
            coords[i,j] = float(a[(j+1)])

N = len(atoms)

N_e = 0
for i in atoms:
    if i == "C":
        N_e += 6
    elif i == "H":
        N_e += 1
    elif i == "F":
        N_e += 9
    elif i == "O":
        N_e += 8
    elif i == "N":
        N_e += 7
    else:
        print("Unexpected Atom")
print(N_e)



ff = np.zeros((20000,N))
for i in range(N):   #initalise form factors
    elastic = np.genfromtxt(atoms[i]+"_elastic.dat")
    ff[:,i] = np.sqrt(elastic[:,1])

def scattering(q,coords):
    s = np.zeros(20000)
    for i in range(N):
        s += ff[:,i]**2
        for j in range(i):
            R_i = coords[i,:]
            R_j = coords[j,:]
            R = np.linalg.norm(R_i-R_j) / 0.52917
            s += 2*ff[:,i]*ff[:,j]*np.sinc(q*R/np.pi)
    return s

total_inelastic = np.zeros(20000)
for a in atoms: #generate total_inelastic
    total_inelastic += np.genfromtxt(a+"_inelastic.dat")[:,1]
    
q = elastic[:,0]
s = scattering(q,coords) + total_inelastic - N_e
plt.plot(q,s)
X = np.column_stack((q,s))
np.savetxt("IAM.dat", X)
plt.show()
