import numpy as np
import matplotlib.pyplot as plt
import sys
import scipy.fft as ft

#This script plots the percent intensity difference, along with the absolute PDF difference for alternating arguments. i.e. 2nd-1st, 4th-3rd, 6th-5th and so on.
HF = np.genfromtxt(sys.argv[1])
N_e = int(np.roots((1,-1,-HF[0,1]))[0])
print(N_e)

def basiscolour(data_label):
    colour_dict={
            "cc-pVDZ": "tab:blue",
            "cc-pVTZ": "tab:orange",
            "cc-pVQZ": "tab:green",
            "aug-cc-pVDZ": "tab:red",
            "aug-cc-pVTZ": "tab:gray",
            "aug-cc-pVQZ": "tab:cyan"
            }
    return colour_dict.get(data_label)

for i in range(len(sys.argv)//2):
    scat = np.genfromtxt(sys.argv[2*i+2])
    HF = np.genfromtxt(sys.argv[2*i+1])
    x = HF[ :,0] 
    HFsigma = HF[:,1]
    r = ft.rfftfreq(scat.size-1,scat[1,0]-scat[0,0])*np.pi*2
    q = scat[:,0]
    sigma = scat[:,1]
    diff = np.subtract(sigma, HFsigma)
    y = 100*diff/(HFsigma+N_e)
    dq = q[1]-q[0]
    coulombhole = np.zeros_like(r)
    count = 0
    
    for j in r:
        integrand = diff*np.sin(j*q)*j*q
        coulombhole[count] = (1/np.pi)*np.trapz(integrand,dx = dq)
        count += 1
    
    
    
    data_label=input("please input label for "+sys.argv[2*i+2])
    plot1 = plt.figure(1)
    plt.plot(x,y,label=data_label,color=basiscolour(data_label))
    plt.xlabel("$q / a_0^{-1}$",fontsize=12)
    plt.ylabel("$\%\Delta I$",fontsize=12)
    plt.xlim(0, 25)
    plt.legend()
    plt.tight_layout()
    plot2 = plt.figure(2)
    plt.plot(x,coulombhole,label = data_label,color=basiscolour(data_label))
    plt.xlabel("$r_{12} / a_0$",fontsize=12)
    plt.ylabel("$\Delta P(r_{12})$",fontsize=12)
    plt.xlim(0, 25)
    plt.legend()
    plt.tight_layout()
    print(np.linalg.norm(y))

plt.show()


