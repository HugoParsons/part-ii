#This bash script searches for energies in pyscf output files.
export WD=$PWD
for d in $(find $WD -maxdepth 4 -type d)

do
  cd $d
  echo $d >> $WD/energies.out
  grep CASCI\ E pyscf.out >> $WD/energies.out
done
