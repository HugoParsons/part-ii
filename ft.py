import numpy as np, matplotlib.pyplot as plt, scipy.fft as ft
import sys
#this script calculates the PDF of an input scattering pattern
for i in range(len(sys.argv)-1):
    scat = np.genfromtxt(sys.argv[i+1])
    step = scat[1,0]-scat[0,0]
    r = ft.rfftfreq(scat.size-1,step)*np.pi*2
    q = scat[:,0]
    sigma = scat[:,1]
    dq = q[1]-q[0]
    dr = r[1]-r[0]
    pairdist = np.zeros_like(r)
    P_r = np.zeros_like(r)
    count = 0
    for j in r:
        integrand = sigma*np.sin(j*q)*q
        P_r[count] = (1/np.pi)*np.sum(integrand*dq)
        pairdist[count] = P_r[count]*j
        count += 1

    coulombenergy = np.sum(P_r*dr)

    area = np.trapz(pairdist, dx = dr)
    print("area = " + str(area))
    print("Coulomb Repulsion Energy = "+ str(coulombenergy))
    plt.plot(r,pairdist)


plt.xlabel("$r_{12} / a_0 $",fontsize=12)
plt.ylabel("$P(r_{12})$",fontsize=12)
plt.tight_layout()
plt.savefig("ft.pdf",format="pdf")
plt.show()
