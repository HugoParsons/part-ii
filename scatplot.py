import numpy as np
import matplotlib.pyplot as plt
import sys
# a simple plotter for scattering data
for i in range(len(sys.argv)-1):
    data = np.genfromtxt(sys.argv[i+1])
    x = data[ :,0]
    y = data[ :,1]
    plt.plot(x,y,label=sys.argv[i+1])

plt.xlabel(" $q / a_0^{-1}$",fontsize=12)
plt.ylabel("$I$",fontsize=12)
plt.tight_layout()
plt.show()
